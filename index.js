//By convention, class names should begin with an uppercase character
class Student {
	//the constructor method defines how objects created from this class will be assigned their initial property values
	constructor(name, email, grades){
		this.name = name
		this.email = email
		this.gradesAve = undefined
		this.pass = undefined
		this.honor = undefined

		if(grades.length ===4){
			if(grades.every(grade => grade >= 0 && grade <= 100)){
				this.grades = grades
			} else{
				this.grades = undefined
			}

		}else{
			this.grades = undefined
		}

	}
	login(){
		console.log(`${this.email} has logged in.`)
		return this
	}
	logout(){
		console.log(`${this.email} has logged out.`)
		return this
	}
	listGrades(){
		console.log(`${this.name}'s quarterly averages are: ${this.grades}`)
		return this
	}
	computeAve(){
		let sum = 0
		this.grades.forEach(grade => sum = sum + grade)
		this.gradesAve = sum/4
		return this
	}
	willPass(){
		this.pass = this.computeAve() >= 85 ? true : false;
		return this
	}
	willPassWithHonors(){
		if(this.gradesAve >= 90){
			this.honor = true
		}else if(this.gradesAve < 90 && this.gradesAve >= 85){
			this.honor = false
		}else if(this.gradesAve < 85){
			this.honor = undefined
		}
		return this
	}



}


//instantiate/crete objects from our Student class
let studentOne = new Student("John", "john@mail", [89, 84, 78, 88])
let studentTwo = new Student("Joe", "joe@mail.com", [78, 82, 79, 85])
let studentThree = new Student ("Jane", "jane@mail.com", [87, 89, 91, 93])
let studentFour =  new Student("Jessie", "jessie@mail.com", [91, 89, 92, 93])

// console.log(studentOne)
// console.log(studentTwo)
// console.log(studentThree)
// console.log(studentFour)